package com.mahdikaseatashin.happyplaces.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mahdikaseatashin.happyplaces.R
import com.mahdikaseatashin.happyplaces.adapters.HappyPlacesAdapter
import com.mahdikaseatashin.happyplaces.databases.DataBaseHelperHappyPlaces
import com.mahdikaseatashin.happyplaces.models.HappyPlaceModel
import com.mahdikaseatashin.happyplaces.utils.SwipeToDeleteCallback
import com.mahdikaseatashin.happyplaces.utils.SwipeToEditCallback
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    companion object {
        const val ADD_ITEM_CODE = 1
        const val MODEL_TAG = "Tag"
        const val LEFT = 1
        const val RIGHT = 2
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getHappyPlacesListFromLocalDB()

        floating_action_button_add.setOnClickListener {
            val intent = Intent(this, AddHappyPlacesActivity::class.java)
            startActivityForResult(intent, ADD_ITEM_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ADD_ITEM_CODE && resultCode == Activity.RESULT_OK) {
            getHappyPlacesListFromLocalDB()
        }
    }


    private fun setUpHappyPlacesRecyclerView(happyPlaceList: ArrayList<HappyPlaceModel>) {
        recycler_view_items.layoutManager = LinearLayoutManager(this)
        recycler_view_items.setHasFixedSize(true)
        val placesAdapter = HappyPlacesAdapter(this, happyPlaceList)
        recycler_view_items.adapter = placesAdapter
        placesAdapter.setOnClickListener(object : HappyPlacesAdapter.OnClickListener {
            override fun onClick(position: Int, model: HappyPlaceModel) {
                val intent = Intent(this@MainActivity, HappyPlacesDetail::class.java)
                intent.putExtra(MODEL_TAG, model)
                startActivity(intent)
            }
        })
        val editSwipeHandler = object : SwipeToEditCallback(this@MainActivity) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val adapter = recycler_view_items.adapter as HappyPlacesAdapter
                adapter.notifyEditItem(
                    this@MainActivity,
                    viewHolder.adapterPosition,
                    ADD_ITEM_CODE,
                    RIGHT
                )
            }
        }
        val deleteSwipeHandler = object : SwipeToDeleteCallback(this@MainActivity) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val adapter = recycler_view_items.adapter as HappyPlacesAdapter
                adapter.notifyEditItem(
                    this@MainActivity,
                    viewHolder.adapterPosition,
                    ADD_ITEM_CODE,
                    LEFT
                )

            }
        }
        val itemTouchHelper2 = ItemTouchHelper(deleteSwipeHandler)
        itemTouchHelper2.attachToRecyclerView(recycler_view_items)
        val itemTouchHelper = ItemTouchHelper(editSwipeHandler)
        itemTouchHelper.attachToRecyclerView(recycler_view_items)

    }

    private fun getHappyPlacesListFromLocalDB() {
        val dbHandler = DataBaseHelperHappyPlaces(this)
        val getHappyPlaceList: ArrayList<HappyPlaceModel> = dbHandler.getHappyPlaces()
        if (getHappyPlaceList.size > 0) {
            recycler_view_items.visibility = View.VISIBLE
            tv_not_found.visibility = View.GONE
            setUpHappyPlacesRecyclerView(getHappyPlaceList)
        } else {
            recycler_view_items.visibility = View.GONE
            tv_not_found.visibility = View.VISIBLE
        }
    }
}
