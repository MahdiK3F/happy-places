package com.mahdikaseatashin.happyplaces.databases

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import com.mahdikaseatashin.happyplaces.models.HappyPlaceModel

class DataBaseHelperHappyPlaces(context: Context) : SQLiteOpenHelper(
    context, DB_NAME, null,
    DB_VERSION
) {
    companion object {
        const val TB_HAPPY_PLACE = "HappyPlaceTable"
        const val DB_NAME = "HappyPlacesDatabase"
        const val DB_VERSION = 1

        const val KEY_ID = "id"
        const val KEY_TITLE = "title"
        const val KEY_DESCRIPTION = "description"
        const val KEY_DATE = "date"
        const val KEY_LOCATION = "location"
        const val KEY_LATITUDE = "latitude"
        const val KEY_LONGITUDE = "longitude"
        const val KEY_IMAGE = "image"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(
            "CREATE TABLE $TB_HAPPY_PLACE ( " +
                    "$KEY_ID INTEGER PRIMARY KEY," +
                    "$KEY_TITLE TEXT," +
                    "$KEY_DESCRIPTION TEXT," +
                    "$KEY_DATE TEXT," +
                    "$KEY_LOCATION TEXT," +
                    "$KEY_LATITUDE DOUBLE," +
                    "$KEY_LONGITUDE DOUBLE," +
                    "$KEY_IMAGE TEXT)"
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS $TB_HAPPY_PLACE")
        onCreate(db)
    }

    fun deleteHappyPlace(happyPlaceModel: HappyPlaceModel): Int {
        val db = this.writableDatabase
        val result = db.delete(TB_HAPPY_PLACE,"${KEY_ID} = ${happyPlaceModel.id}",null)
        db.close()
        return result
    }

    fun addHappyPlace(happyPlaceModel: HappyPlaceModel): Long {

        val db = this.writableDatabase
        val contentValue = ContentValues()
        contentValue.put(KEY_TITLE, happyPlaceModel.title)
        contentValue.put(KEY_DESCRIPTION, happyPlaceModel.description)
        contentValue.put(KEY_DATE, happyPlaceModel.date)
        contentValue.put(KEY_LOCATION, happyPlaceModel.location)
        contentValue.put(KEY_LATITUDE, happyPlaceModel.latitude)
        contentValue.put(KEY_LONGITUDE, happyPlaceModel.longitude)
        contentValue.put(KEY_IMAGE, happyPlaceModel.image)

        val result = db.insert(TB_HAPPY_PLACE, null, contentValue)

        db.close()

        return result
    }

    fun updateHappyPlace(happyPlaceModel: HappyPlaceModel): Int {

        val db = this.writableDatabase
        val contentValue = ContentValues()
        contentValue.put(KEY_TITLE, happyPlaceModel.title)
        contentValue.put(KEY_DESCRIPTION, happyPlaceModel.description)
        contentValue.put(KEY_DATE, happyPlaceModel.date)
        contentValue.put(KEY_LOCATION, happyPlaceModel.location)
        contentValue.put(KEY_LATITUDE, happyPlaceModel.latitude)
        contentValue.put(KEY_LONGITUDE, happyPlaceModel.longitude)
        contentValue.put(KEY_IMAGE, happyPlaceModel.image)

        val result =
            db.update(TB_HAPPY_PLACE, contentValue, KEY_ID + "=" + happyPlaceModel.id, null)

        db.close()

        return result
    }

    fun getHappyPlaces(): ArrayList<HappyPlaceModel> {
        val happyPlaceList = ArrayList<HappyPlaceModel>()
        val selectQuery = "SELECT * FROM $TB_HAPPY_PLACE"
        val db = this.readableDatabase
        try {
            val cursor: Cursor = db.rawQuery(selectQuery, null)
            if (cursor.moveToFirst()) {
                do {
                    val place = HappyPlaceModel(
                        cursor.getInt(cursor.getColumnIndex(KEY_ID)),
                        cursor.getString(cursor.getColumnIndex(KEY_TITLE)),
                        cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION)),
                        cursor.getString(cursor.getColumnIndex(KEY_DATE)),
                        cursor.getString(cursor.getColumnIndex(KEY_LOCATION)),
                        cursor.getString(cursor.getColumnIndex(KEY_IMAGE)),
                        cursor.getDouble(cursor.getColumnIndex(KEY_LATITUDE)),
                        cursor.getDouble(cursor.getColumnIndex(KEY_LONGITUDE))
                    )
                    happyPlaceList.add(place)
                } while (cursor.moveToNext())
            }
            cursor.close()
        } catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }
        return happyPlaceList
    }
}
